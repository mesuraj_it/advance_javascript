//Arrow Function syntax
//syntax of regular function
    /*const callme = function (){
        console.log("Syntax of regular anonymus function ")
    }*/

//converting it to an arrow function
/*const callme = ()=>{
    console.log("Syntax of arrow function");
}
callme();*/

//function return something
    /*const greet =function(){
        return "good morning";
    }*/
        //OR
    //const greet = ()=> "good morning"
    //return an object we need to add parenthesis and curly braces
    //const greet =()=>({name:"suraj"});


    //for one parameter we dont need to use parenthesis but for more than one parameter we need to use parenthesis 
    /*const greet = name => "good morning " +name ;
    console.log(greet('suraj'));*/
                //OR
    const greet = (name, end) => "good morning " +name +  end;
    console.log(greet('suraj','goobye'));

//great();//it will not give any output
//console.log(greet());