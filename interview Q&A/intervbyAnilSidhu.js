let arr = [4,8,6,4,1,7,6,9];
//console.log(arr);

//cut array length to 3, show only first 3 element
/*arr.length=3;
console.log(arr);*/


//sum of array
/*let sum = arr.reduce((a,b)=>a+b)  //a*b for multiply
console.log(sum);*/


//remove duplicate values from array
/*let unique = new Set(arr);
console.log(unique);//output as like object
console.log([...unique]);*/



//value swap
let a = 20; b = 50;
[a,b] = [b,a];
console.log(a,b);

// swap without using third variable and advance js function
/*let a=25; b=55;
a = a+b//80
b = a-b//25
a = a-b
console.log(a,b);*/

