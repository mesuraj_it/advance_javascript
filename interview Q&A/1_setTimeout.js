/*---------Question 1-------------Namaste javaScript--------*/
//print 1,2,3,4,5 along with increment the seconds
/*function x(){
    for(let i=1; i<=5; i++){
        setTimeout(() => {
            console.log(i);
        },i * 1000)
    }
}
*/

//if interviewer say not to use let, use var then (setTimeout + closure)
/*function x(){

    for(var i=1; i<=5; i++){
        function close(i){  //if its look confuse then you can pass close(x) x is a reference of i
            setTimeout(() => {
                console.log(i); //(x)
            },i * 1000)         //x * 1000
        }
        close(i);
    }
    console.log("Namaste Javascript");
}

x();*/


 

/*function x(){
    for(var i=1; i<=5; i++){
        setTimeout(()=>{
            console.log(i);
        },i * 1000)
    }
}

x();*/

//var has function scope. var can not be overridden in a function
//var overwrite values if declares outside function
var username = "john";
/*function name(){
    var username = "tom";
    //console.log(username);
}*/
if(true){
    var username = "tom";
    console.log(username)
}
console.log(username);



