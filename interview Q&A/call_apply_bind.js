//call, apply, bind

//problem statement
/*let userdetails = {
    name : "Ajay suneja",
    age: 27,
    designation: "web developer",
    printdetails: function(){
        console.log(this.name);
    }
}
userdetails.printdetails();


let userdetails2 = {
    name : "suraj roy",
    age: 30,
    designation: "software developer",
    //if we want to use this function here also then instead of copy or write the same function again we can use call() to point in this.
   /* printdetails: function(){
        console.log(this);
    }*/
/*}
//function borrowing
userdetails.printdetails.call(userdetails2);*/

/*************Another method for global scope of function*********************** */
const printdetails = function(state,country){
    console.log(this.name+" "+state+" "+country);
}


let userdetails = {
    name : "Ajay suneja",
    age: 27,
    designation: "web developer", 
}
printdetails.call(userdetails, "Delhi","India");


let userdetails2 = {
    name : "suraj roy",
    age: 30,
    designation: "software developer", 
}

printdetails.call(userdetails2,"Kolkata","India");

printdetails.apply(userdetails2,["Kolkata","India"]);//we can pass parameters as an array,so we use apply