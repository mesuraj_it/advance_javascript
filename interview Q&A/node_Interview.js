/*==============Node Js Interview  By Mahesh Kariya============================= */
//Q1. Make a promise function true when it is resolved and false when it is rejected

/*const callme = new Promise((resolve, reject)=>{
    if(true){
        resolve("success");
    }else{
        reject();
    }
})*/


/*callme
    .then(result => {
        console.log("All set");
    }).catch(err => {
        console.log("something wrong");
    })*/


/*===================================*/
//Q2.Do the same thing using async await
const  callme = async () =>{ 
    if(true){
        return "hello world";
    }
}

const parent = async ()=>{
    return await callme()
}

(async () =>{
    console.log((await parent()))
})();

//Q3 what is the concept of async await
/*to wait for the callback till then stop the execution for the other functions, once we get reset then we have to proceed
   /* async is a keyword to let the function knows that it is going to be await /async denotes that it is going to be await-like this
   /*await can't be used without async or inside a normal function

//Q3. difference between get and post
GET typically has relevant information in the URL of the request |POST typically has relevant information in the body of the request
GET retrieves a representation of the specified resource.          |POST is for writing data, to be processed to the identified resource.
We can bookmark GET request                                        |Can not bookmark POST request
It is less secure because data sent is part of the URL             |It is a little safer because the parameters are not stored in browser
                                                                     history or in web server logs*/
//Q4.Mention or explain how JWT works
import jwt from 'jsonwebtoken'


//Q5.Mak a POST api
import express from 'express'
import { request } from 'http';
const app = express()
app.use(express.json())
app.use (express.urlencoded({extended:true}))

app.post('/',(req,res)=>{
    res.json(req.body)
})
app.listen()

//Q6.What is Middleware, mention by code
/*middleware is anything that execute first before execute something we want.Suppose we want to ececute this post request*/

import express from 'express'
import { request } from 'http';
import { connected } from 'process';
import { resolve } from 'path';
import { fstat } from 'fs';
const app = express()
app.use(express.json())
app.use (express.urlencoded({extended:true}))
//create this middleware in somewhere
const myMiddleware= (req, res, next)=>{
    console.log("something");
    next()
}

app.use(myMiddleware())
app.post('/',(req,res)=>{
    res.json(req.body)
})
app.listen()


//Q7.what is call bind & apply

//Q8. what is closure 
    /*we can access a parent function variable in a child function*/

//Q9.what is spred operator?

//Q10.difference between let, const and var

//what will be the output?
function calculatesalary(job){
    var income =5;
    if(job){
        var income = 50;
    }

    {
        var income =500;
        {
            var income =5000;
        }
    }
    return income;
}

console.log(calculateSalary(true));
//output will be 5000 bcoz var has global access

//Q11.Have you ever write testcases ?

//Q12.what is arrow function?
 /*One thing I notice that if we write arrow function there will be a single line of code, we dont need to write await and curly braces sometimes,
  we can simply write async,that is the benefit of arrow function*/

//Q13.What is callback function?
    /*A callback function is called after a given task. It allows other code to be run in the meantime and prevents any blocking.  Being an asynchronous platform, Node.js heavily relies on callback. All APIs of Node are written to support callbacks.*/

//Q14.Node.js is single-threaded for async processing. By doing async processing on a single-thread under typical web loads, more performance and scalability can be achieved instead of the typical thread-based implementation.

//Q15. Why use Node Js?
    /*It is generally fast
    It rarely blocks
    It offers a unified programming language and data type
    Everything is asynchronous 
    It yields great concurrency*/

//Q16.What are the advantages of using promises instead of callbacks?
    /*The control flow of asynchronous logic is more specified and structured.
    The coupling is low.
    We've built-in error handling.
    Improved readability.*/


//Why mongodb is more popularly used with Node.js?
    /*It is a NoSQL, cross-platform, document-oriented database that provides high performance, 
    high availability, and easy scalability.*/

//What is an Event Loop in Node.js?
    /*Event loops handle asynchronous callbacks in Node.js. It is the foundation of the non-blocking input/output in Node.js, 
    making it one of the most important environmental features.*/

//What are the two types of API functions in Node.js?
    /*he two types of API functions in Node.js are:
    Asynchronous, non-blocking functions
    Synchronous, blocking functions*/

// How do we implement async in Node.js?
    async function fun1(req, res){
        let response = await request.length('http://localhost:3000');
        if(response.err){
            console.log('error');
        }else{
            console.log('fetched response');
        }
    }
//What is REPL in Node.js?
    /*REPL stands for Read Eval Print Loop, and it represents a computer environment. 
    It’s similar to a Windows console or Unix/Linux shell in which a command is entered. 
    Then, the system responds with an output */

//reate a simple server in Node.js that returns Hello World
    const express = require('express');
    const app = express();

    app.get('/', function(req,res){
        res.send('Hello World');
    })
    app.listen(3000,()=>{
        console.log(`Server is running on http://localhost:3000`);
    })

//what is stream in node js?

//what is buffer in node js?
    /*Buffer class stores raw data similar to an array of integers but corresponds to a raw memory allocation outside the V8 heap. 
    Buffer class is used because pure JavaScript is not compatible with binary data*/

//How do you open a file in Node.js?
    fs.open( filename, flags, mode, callback );

    // Open file demo.txt in read mode
    fs.open('demo.txt', 'r', function (err, f) {
        console.log('Saved!');
    });


//What is mongo db?
    /*MongoDB is NoSQL database written in C++ language. It uses JSON-like documents with optional schemas.
    It provides easy scalability and is a cross-platform, document-oriented database.
    MongoDB works on the concept of Collection and Document.
    It combines the ability to scale out with features such as secondary indexes, range queries, sorting, aggregations, and geospatial indexes.*/

    1.what is Node Js?
    Node Js is Primarily a server-side scripting language
    It is very popular bcoz it supports non-blocking(activities) input output operation

2.Difference between JavaScript and Node js?
    JavaScript is a Programing language || Node Js is a Scripting language
    JavaScript Used for client Side operations || Node used for server side operation

3.How Node js works?
    Node Js works on the single threaded model to ensure that there is support for asynchronus processing. with this, it makes it scalable and efficiency under high amounts of load.

4.Disadvantages of Node js?
    A multi threaded platfrom can run more effectively and provide better responsiveness
    It still using relational database.

5.Different API Function supported in node js?
    Synchronous APIs: Used for non-blocking functions
    asynchronus APIs: Used for blocking functions

6.Difference between Synchronous and asynchronus?

7.What is event loop in node js?
    The event loop is what allows Node.js to perform non-blocking I/O operations. The Node.js JavaScript code runs on a single thread. There is just one thing happening at a time

9.what are the modules we use in node js?
	Node.js includes three types of modules:
		1.Core Modules - Node.js has many built-in modules that are part of the platform and comes with Node.js installation
		syntax to use it: const module = require('module_name');
		
		2.Local Modules - local modules are created locally in your Node.js application
		syntax to use:		exports.add = function (x, y) { 
								return x + y; 
							}; 
							
		3.Third Party Modules - Third-party modules are modules that are available online using the Node Package Manager(NPM).
		syntax to use:	npm install express
						npm install mongoose
		
10.what are some core modules in node js?

	RPL- Read Eval Print Loop
	events - To handle events
	assert - Provides a set of assertion tests
	buffer - To handle binary data
	stream - To handle streaming data
	querystring - To handle URL query strings
	http- http module includes classes, methods and events to create Node.js http server.
	url - url module includes methods for URL resolution and parsing.
	path - path module includes methods to deal with file paths.
	process - provides information and control about the current Node.js process.
	fs - fs module includes classes, methods, and events to work with file I/O.
	
	In order to use Node.js core or NPM modules, you first need to import it using require() function as shown below.
	var module = require('module_name');
