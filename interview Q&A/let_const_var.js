//let const is block scope. we can access these keyword only in block like if else, loop(for, foreach)
const age = 18;
if(age>=18){
    var driving = true; //polute codes, can access from outside
   // let driving = true; //not accessable from outside
    //const driving = true; //not accessable from outside
}
console.log(driving);
//if we use var here then it is accessable outside of the if block which polutes our codes
//let and const only accessable inside the block if it is declared in block


//var has function scope, global and local scope. If we declare var in a function, then it cannot be accessable outside of the function
//example:-*/
function register(){
    var username = 'rakesh';
    var password ='123';
} 
console.log(username);
console.log(password);

//we can create multiple variable with same name using var
var product = "laptop";
var product ="computer";
console .log(product)//it will override other variable by the last one

//but let dont allow to create multiple variable with same name
let phone = "apple";
let phone ="moto";
console.log(phone);//showing error. cannot be redeclare