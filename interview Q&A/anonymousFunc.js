//what is anonymous Function
//Function without name is called anonymus functoin
//we can assign a function in a variable so we dont need to give the name of the function becoz we call the function by the variable 
//syntax
/*const showme = function(){    //it is also called a function expression
    console.log("anonymus");
}
showme();   //called function*/


//why we use anonymus function and when to use anonymus function
 /* 1.when we need to execute a function only one time
    2.if we want to pass a function as a parameter in another function only for one time
    3.it will not store in memory as it has no name*/
    /*const callme = function(){
        console.log("Hello")
    }*/

    //setTimeout(functionname, time)
    //We pass the above function  as a parameter
    setTimeout( function(){
        console.log("Hello")
    }, 2000)