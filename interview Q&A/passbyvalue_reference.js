/*there are two types of datatypes in javascript
 *   1.Primitive datatype--numbers, strings, booleans, null, undefined are primitive values(pass by values)

  *  2.Non-primitive datatype-- objects such as functions and arrays are refered to as non-primitive values(pass by reference).

    Difference:primitives are immutable and non-primitives are mutable
    there is no way to change a primitive value once it gets created
*/
//ex: Pass by value
let a = 5;
let b = a;//the value of a pass in b
 b= a + 5;

 //both a and b can work independently
 console.log(a);
 console.log(b);



 //Pass by reference object example
 let obj1 ={
            "name":"ajay",
            "age":"20"
        }
let obj2 = obj1;    // pass reference

obj2.age = "22";    //we change the value of age for obj2 but it changed the value of age for obj1 and obj2 both

console.log(obj1);
console.log(obj2);


//Pass by reference array example
let arr1 = [1,2,3];
let arr2 = arr1;    //pass reference

arr2.push(4);   //we insert a new data in arr2 but it changed also arra1 

console.log(arr1);
console.log(arr2);