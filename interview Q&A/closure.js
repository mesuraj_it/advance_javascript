/**
 * A closure is the combination  of a function and the lexical environment within which that function was declared
 * closure in action that is inner function can have access to the outer function variables as well as the global variables.
 * 
*/
/*const outerfunc = (a) =>{
    let b = 10;
    const innerfunc = () =>{
        let sum = a + b;
        console.log(`the sum of two is ${sum}.`);
    }
    innerfunc();
}

outerfunc(5);*/



//function without name is called annonymus function
//closure by Ajay Suneja
/*var sum=function(a){
    console.log('Hello Viewers ' +a);
    var c =4;
    return function(b){
        return a+b+c;   //scope of a and c variable is called lexical scope
    }
}
 var store = sum(2);
 console.log(store(5));*/

 const outerfunc = (a)=>{
    let b=10;
    const innerfunc =()=>{
        let sum = a+b;
        console.log(sum);
    }
    innerfunc();
 }
outerfunc(5);