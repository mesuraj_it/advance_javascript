/*---------Question 2------By-------coder's gyan--------*/

//if use let;
/*for(let i=0; i<5; i++){
    setTimeout(()=>{
        console.log(i);
    },2000);
}*/
//output: 0 1 2 3 4

//if use var
/*for(var i=0; i<5; i++){
    setTimeout(()=>{
        console.log(i);
    },2000);
}*/
//Output: 5 5 5 5 5
//How setTimeout() works here


/*-----------------------------------*/
//function scope & block scope
//let has block scope
//var has function scope

// var user = 'john';
let user = 'john';

// function scope
/*function login(){
    var user = 'tom'
}*/
//block scope
/*if(true){
    //var user = 'tom'
    let user = 'tom';
    console.log(user);
}


console.log(user);*/


/*-----------------------------------*/
// solve the problem by using var instead of let
for(let i=0; i<5; i++){
    /*function test(i){       //this is called closure
        setTimeout(()=>{
            console.log(i);
        },2000);
    }
    test(i);*/
    //OR
    //this is called immediately invoked function expression
    ((i)=>{
        setTimeout(()=>{
            console.log(i);
        },2000);
    })
    
    
   
}

