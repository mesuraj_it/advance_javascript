// ES6 - Rest and Spread Operator

//Rest in es6
/*function addnumbers(a,b,c,...other){
    console.log(other);  //Rest combine all element in array what we pass in arguement 
    return a+b+c;
}
const res =addnumbers(2,7,5,1,9,14,12,3);
console.log(res);*/

//OBJECT ke saath-Rest
    const students ={
        name:"Ajay",
        age:"28",
        hobbies:["cricket","singing"]
    }
    //console.log(students);
    /*const age =students.age;
    console.log(age);*/

    //array destructuring
    /*const {age,name} =students;
    console.log(age,name);*/
    
    /*const {...rest}=students;
    console.log(rest);*/

    const {...rest}=students;   //const {age,...rest}=students
    console.log(rest)
/*************************************/


//Spread Operator  (... variable name)
/*const names =["Ajay","Bijay","Rahul","Suman","vivek"];
function getnames(name1, name2, name3, name4, name5){
    console.log(name1,name2,name3,name4,name5);
}
getnames(...names)*/
    //object ke saath

    /*const newStudents ={
        ...students,
        age:"30"
    }
    console.log(newStudents);*/



    /*---------------Spread---By-----coder's--Gyan-------array------------------------*/
    const lang =["c", "c++", "PHP", "JAVA"];
    //const newLang =[...lang];
    const newLang =["typescript",...lang];

    console.log(newLang);

    //Spread example of object
    const settings ={
        volume: 10,
        bright: 80,
    }

    //const newSettings = settings//by reference, it points same (address) memory location
    const newSettings ={...settings}