// Find if the string is palindrome or not
//Example of palindrome-->dad, noon, level, madam, abcba, 12321




// Step-1  Using javascript inbuild function
//reverse(), split(""), join(""), toLowercase()

function isPalindrome(str){
//let reverseStr = str.split(""); //splits all the character of given string/number 
//let reverseStr = str.split("").reverse();   //print string/number into reverse order
let reverseStr = str.split("").reverse().join("");  //join all letters/characters/numbers and remove any seperation character like(- , "" '' space _ =)
    //console.log(reverseStr); 
    return  str.toLowerCase() === reverseStr.toLowerCase(); //it will return true or false
}
//const res = isPalindrome("madam");
//console.log(res);
    //OR
//console.log(isPalindrome("madam"));




//Step-2 without inbuild function
//will match left char with right char

  function isPal(str2){
        let newStr = str2.toLowerCase();

        let left = 0;
        let right = newStr.length -1;

        while(left < right){
            // if(newStr[left] !== newStr[right]){
            //     return false;
            // }
                        //OR
            if(newStr[left] !== newStr[right])
                return false;
                
            left++;
            right--;
        }
        return true

  }
  console.log(isPal("levelk"));