//Q1. print or return 3rd highest and 3rd smallest number in and array
let myarr = [17,21,36,22,15,43,45];

/*****1st way using index*****/
//console.log("3rd smallest element", myarr.sort()[2]);
//OR
//console.log("3rd smallest element", myarr.sort().reverse()[myarr.length-3]);


//console.log("3rd largest element", myarr.sort()[myarr.length-3]);
                //OR
//console.log("3rd largest element", myarr.sort().reverse()[2]);



//Q2. Find largest and 3rd largest element in an array without using index
    const arr = [11, 2, 44, 7, 6, 25, 19, 47, 10];

    //largest number & 3rd largest number print
   /* let largest = arr[0];
    let thirdlargest = arr[0];

    for(let i=0; i<arr.length; i++){
        if(arr[i] > largest){
            largest = arr[i];
        } else if(arr[i] > thirdlargest){
            thirdlargest = arr[i];
        }
    }
    console.log(largest,thirdlargest);*/


    //Using function
   /* const findthirdlargest =(arr = []) =>{
        let largestnumber = arr[0];
        let thirdlargest = arr[0];

        for(let i =0; i<arr.length; i++){
            if(arr[i] > largestnumber){
                largestnumber = arr[i];
            } else if(arr[i] > thirdlargest){
                thirdlargest = arr[i];
            }
        }
        return thirdlargest;
    }
  const print = findthirdlargest(arr);
  console.log(print);*/


  //Q2. Find smallest and 3rd smallest element in an array without using index
  let smallest = arr[0];
    let thirdsmallest = arr[0];

    for(let i=0; i<arr.length; i++){
        if(arr[i] < smallest){
            smallest = arr[i];
        } else if(arr[i] < thirdsmallest){
            thirdsmallest = arr[i];
        }
    }
    console.log(smallest,thirdsmallest);