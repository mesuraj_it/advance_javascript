let names =["a","b","c","d","e"];
console.log(names.indexOf("c"));    //output=> 2 index of the element
console.log(names.includes("f"));   // output=> false the element is present in the array or not, return true or false
console.log(names.slice(1));    //output=>["b","c","d","e"]  cut the first element