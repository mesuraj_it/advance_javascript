//insertion sort
//[8,2,4,1,3];
//consider second element is smallest element of athe array, then compare with left side element of the array
//take a variable which stores the current element as smallest numbr in the array

const arr =[8,2,4,1,3];

// function sorted(arr){
//     for(let i=1; i<arr.length; i++){
//         let cur = arr[i];
//         //console.log(i,cur);
//         let j = i-1;
//         while(j>=0 && j>cur){
//             arr[j+1] = arr[j];
//             j--;
//         }
//         arr[j+1] = cur;
//     }
//     return arr;
    
// }
// const res = sorted(arr);
// console.log(res);

//[7,3,8,5,2,9,4,6];

//function syntax in ES6
    const sorted=(arry)=>{
        for(let i=1; i<arry.length; i++){
            let cur = arry[i];       //arr[i] prints the element of array = 2
            let j = i-1;        //i prints the position of the element in an array
            while(arry[j]>cur){
                arry[j+1] = arry[j];
                j--;
            }
            arry[j+1] = cur;
            //console.log(arry[j+1]);
        }
        return arry;
    }

    const res = sorted([7,3,8,5,2,9,4,6,1]);
    console.log(res);