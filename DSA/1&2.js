//checking the sum of two pair is zero
//[-5,-4,-3,-2,0,2,4,6,8]->Input
//The first two number (-4+4=0)->Output
//logic 1, Solution 1
function getSomePairZero(arr){
    for(let number of arr){
       
        console.log("Outer Loop");
         for(let j=1; j<arr.length; j++){
             console.log("Innr loop");
            if(number + arr[j] === 0){
                return [number, arr[j]];
            }
         }
    }
}

//const result = getSomePairZero([-5,-4,-3,-2,0,2,4,6,8]);
//console.log(result);

//o(n^2) quadratic time complexity(2 for loop is running)

//logic 2, another solution, this logic is applicable only for the condition when array is sorted, otherwise we have to sort the array first.
function twosumzero(array){
    let left = 0;       //index of left number -5= array[0]
    let right = array.length-1;     //index of right number 8=array[8]
    while(left < right){
        sum = array[left] + array[right];
        if(sum===0){
            return [array[left],array[right]];
        } else if(sum>0){
            right--;
        } else {
            left++;
        }
    }
}
const result2 = twosumzero([-5,-4,-3,-2,0,2,4,6,8]);
//console.log(result2);
//O(n) linear time complexity
