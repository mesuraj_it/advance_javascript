//Bubble Sort
//A Sorting Algorithm where the largest values bubble up at the top(arrange the number shortest to largest)

//[5,3,4,1,2,8,6,7]
//array length = 8, index 0-7
/*
    i  j+1
-------------
5-  8- 0+1
3-  7- 1+1   
4-  6- 2+1
1-  5- 3+1
2-  4- 4+1
8-  3- 5+1
6-  2- 6+1
7-  1- 7


*/

function bubbleSort(array){
   // let arr2 =[];
    for(let i=array.length; i > 0; i-- ){
        //console.log (arr2.push(i));
        for(let j=0; j<i-1; j++){
            if(array[j] > array[j+1]){
                [array[j],array[j+1]] = [array[j+1],array[j]];
            }
        }
    }
    //return arr2;
    return array;
}
const res = bubbleSort([18,33,14,11,22,15,26,17]);
console.log(res);
