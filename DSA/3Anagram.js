//condition
//length check(for both string)
//String 1 'hello'  compare with string 2 'llheo'
//{ h:0, e:0, l:0, o:0}

function isAnagram(string1,string2){
    if(string1.length!=string2.length){
        return false;
    }
    let counter={}
    for(let letter of string1){
        //console.log(letter); // Print the string 1 =  h e l l o
        counter[letter]=(counter[letter] || 0) + 1;     //first time it is undefined, so we set the value 0+1. if it is not undefined, then it increase its previous value + 1  
        //console.log(counter[letter]);   //1 1 1 2 1
    }
    //console.log(counter);   //{ h: 1, e: 1, l: 2, o: 1 }

    for(let items of string2){
        // console.log(items);
        // console.log(counter[items]);
        if(!counter[items]){
            return false;
        }
        counter[items]-=1.
    }
    return true;

}



//isAnagram('hello','llheo');
const check = isAnagram('hello','llheo');
console.log(check);