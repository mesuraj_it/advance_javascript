//find out the length of longest sequence substring in a string, without repeat.

//slide methode
function longestSubstring(s){
    if(!s){
        return 0;
    }
    let end = 0;
    let start = 0;
    let maxlength =0;

    let uniqcharcters = new Set();  //Set() stores unique values
    while(end < s.length){
        if(!uniqcharcters.has(s[end])){
            uniqcharcters.add(s[end]);
            end++;
            maxlength=Math.max(maxlength,uniqcharcters.size);
        } else{
            uniqcharcters.delete(s[start]);
            start++;
        }
    }
    return maxlength;

}
 console.log(longestSubstring("abcabcbb"));