//selection sort
//[0,3,22,15,7,10,34]

function selectionSort(array){
    for (let i=0; i < array.length; i++){
        let min = i;        //OR //let min = array[i];
        //console.log(min); OR // /console.log(i);
        for(let j=i+1; j< array.length; j++){
            if(array[j] < array[min]){
                min = j;
            }
        }
        // if(array[i]!==min){
        //     temp = array[i];
        //     array[i] = array[min];
        //     array[min] = temp;
        // }    //OR
        if(i!==min){
            let temp =array[i];
            array[i] =array[min];
            array[min] = temp;
        }
    }
    return array;
}
//selectionSort([0,3,22,15,7,10,34]);
const res = selectionSort([0,3,22,15,7,10,34]);
console.log(res);


//Time complexity O(n^)