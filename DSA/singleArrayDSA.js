//Remove Element From Array in javaScript

//1.Remove the first element
//var arr = [ "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten" ]; 
 //arr.shift();
 //console.log( arr );

//2.Remove the last element of the array javaScript.
//var arr = [ "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten" ]; 
 //arr.pop();
 //console.log( arr );

//3.Remove specific index elements from array javaScript. I want to delete first and last element


var arr = [ "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten" ]; 
 arr.splice(1,3);
 console.log( arr );

let names =["a","b","c","d","e"];
console.log(names.indexOf("c"));    //output=> 2 index of the element
console.log(names.includes("f"));   // output=> false the element is present in the array or not, return true or false
console.log(names.slice(1));    //output=>["b","c","d","e"]  cut the first element
console.log(names.pop(1));

/************DSA********************DSA**********************DSA********************/

//1.print missing values from an array upto 10.
const numbers =[1,3,6,9]; //Output [2,4,5,7,8,10








//2.Print unique elements of an array not repeat
const x=[1,1,2,3,4,7,4,5,6,9,6,8,6,7,9];//output [1,2,3,4,5,6,7,9]






//3.Print repeated elements  //Output [1,4,6,7,9]





//4.Print non repeated elements //Output [2,3,5,8]



//5.Print maximum repeated element of an array //Output 6 3times

/****************************Start Q.5***********************************/

//Q6. print or return 3rd highest and 3rd smallest number in and array
let myarr = [17,21,36,22,15,43,45];

/*****1st way using index*****/
//console.log("3rd smallest element", myarr.sort()[2]);
//OR
//console.log("3rd smallest element", myarr.sort().reverse()[myarr.length-3]);


//console.log("3rd largest element", myarr.sort()[myarr.length-3]);
                //OR
//console.log("3rd largest element", myarr.sort().reverse()[2]);



//Q6.1. Find largest and 3rd largest element in an array without using index
    const arr = [11, 2, 44, 7, 6, 25, 19, 47, 10];

    //largest number & 3rd largest number print
   /* let largest = arr[0];
    let thirdlargest = arr[0];

    for(let i=0; i<arr.length; i++){
        if(arr[i] > largest){
            largest = arr[i];
        } else if(arr[i] > thirdlargest){
            thirdlargest = arr[i];
        }
    }
    console.log(largest,thirdlargest);*/


    //Using function
   /* const findthirdlargest =(arr = []) =>{
        let largestnumber = arr[0];
        let thirdlargest = arr[0];

        for(let i =0; i<arr.length; i++){
            if(arr[i] > largestnumber){
                largestnumber = arr[i];
            } else if(arr[i] > thirdlargest){
                thirdlargest = arr[i];
            }
        }
        return thirdlargest;
    }
  const print = findthirdlargest(arr);
  console.log(print);*/


  //Q6.2. Find smallest and 3rd smallest element in an array without using index
  let smallest = arr[0];
    let thirdsmallest = arr[0];

    for(let i=0; i<arr.length; i++){
        if(arr[i] < smallest){
            smallest = arr[i];
        } else if(arr[i] < thirdsmallest){
            thirdsmallest = arr[i];
        }
    }
    console.log(smallest,thirdsmallest);


/*************************End Q.5********************************** */