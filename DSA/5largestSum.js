//[1,2,3,4,3,5,4,6,7,8]=>total element or length of array is 10
//Count largest sum of Consecutive Digits
//num = 4 consecutive digits
//largest sum 4+6+7+8=25(output) sum of 4 consecutive digits
/*condition
num>array.length->error msg
array.length-num+1  10-4+1=7 (This is the formula of how many times the loop will run)
*/

function findlargest(array,num){
    if(num>array.length){
        throw new Error("number should br less than array");
    } else {
        let max = 0;
        for(let i=0; i<array.length-num+1; i++){
           // console.log(array[i]);
            let tmp=0;
            for(let j=0; j<num; j++){
               // console.log("i"+i+" "+"j"+j);
                tmp += array[i+j]; //i0+j0=0/i0+j1=1/i0+j2=2/i0+j3=3
            }
            if(tmp>max){
                max=tmp;
            }
        }
        return max;
    }
}
const result = findlargest([1,2,3,4,3,5,4,6,7,8], 4);
console.log(result);

// tmp += array[i+j];
/*  arr[i + j] index->element sum(tmp +)
        0 + 0 =  0  -> 1        1
        0 + 1 =  1  -> 2        1+2=3
        0 + 3 =  2  -> 3        3+3=6
        0 + 4 =  3  -> 4        6+4=10
*/