//Divide & Conquerer Technique
//Find index of given no in a sorted array (index of 7)
/*[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
  0|____________|7_________________|14  num=7
  */
//Everything we take only index of the array
//min(index) = 0 element=1
//max(index) = array.length-1  element=15
//midindex = max/2 or (min+max)/2   (0+14)/2=7 index=element 8

//if array[midindex] < number(7 element) we are compare two elements array[midindex=8] < 7
//min = midindex+1  max = remains unchanged
//if array[midindex] > number(7 element)
//max = midindex-1(8-1)=7;    min=0; we got a new array [1,2,3,4,5,6,7]

//min=0; max=6
//midindex = max/2 or (min+max)/2   (0+6)/2=3 index=element 4
//array[midindex] < number(7 element)
//min = midindex+1(3+1)=4 max=6 [5,6,7]

//min=0; max=2
//midindex =   (4+6)/2=5(index)=element 6
//array[midindex] < number(7 element)
//min = midindex+1  max = remains unchanged min=2 max=2 [7]
function binarySearch(array,num){
    let min=0;
    let max=array.length-1;
    
    while(min<=max){
        let midindex = Math.floor((min + max)/2 ); //solve the problems of getting odd number in decimal 17/2=8.5
        console.log("midindex"+midindex+" "+min+"min"+" "+max+"max");
        if(array[midindex] < num){
            min=midindex+1;
        } else if(array[midindex] > num) {
            max=midindex-1;
        } else {
            return midindex;
        }
    }
}

const result = binarySearch([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 7);
console.log(result);


//time complexity binary 0(log(n));