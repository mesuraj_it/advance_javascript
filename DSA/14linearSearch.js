//implementation of Linear Search in JavaScript
//traverse
function linearsearch (arr, key){
    for(let i=0; i<arr.length; i++){
        if(arr[i] === key){
            return i  //if find a match with the key return the index of the element
        }
    }
    return false    //if the loop runs completely and the element doesn't exit, it will return false
}
const res = linearsearch([1,2,7,4,5,6,3,8,9], 7);
//console.log(res);   //output=>2 index of 7





//Global Linear Search
function globallinear(arr2, key2){
    let elements = [];
    for(let i=0; i<arr2.length; i++){
        if(arr2[i] === key2){
            elements.push(i);   //return the index or positions of the keys, output=> [ 2, 6 ]
            //elements.push(arr2[i]);   //return the keys output=> [ 7, 7 ]
            //console.log(elements.push(arr2[i])) ;  //prints number of the key element
        }
    }
    return elements
    
    //return false;
}
const res2 = globallinear([1,2,7,4,5,6,7,8,9], 7);
//console.log(res2);

/*---------Linear Search Algorithm------------------Real Time Problem--------------------------------*/
const users =[
                {name:"ajay", phone:"51984"}, //object
                {name:"sujay", phone:"6549879"}, //object
                {name:"sujit", phone:"897986"}   //object
            ]

function isUserExit(users, val){
    for(let item of users){
        //console.log(item['name'])
        if(item['name'] === val){
            return true;
           //return item['name'];  //print the name of the key,  output=>sujay
         
        }
    }
    return false;
}
//isUserExit(users, "abc");
const output = isUserExit(users, "sujay");
console.log(output);

// Time Complexity = linear O(n)