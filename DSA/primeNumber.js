//Prime number
//number which is divisible only with 1 and itself, that is called prime number 2, 3, 5, 7, 9, 11, 13


//check if the given number is prime or not
// function check whether a number is prime or not

function isPrime(n){
    //check the given number is grater than 2 or not
    if(n < 2)
        //return `${n} is not a prime number`;
        return false;

     // Check from 2 to n-1
    for(let i = 2; i < n; i++){
        //console.log(i);
        if(n % i === 0){
            //return `${n} is not Prime number`;
            return false;
        }
    }
    //return `${n} is prime`;
    return true;
}
//console.log(isPrime(14));

//---------------by using twofunction, calling onr function into another function-----------------//

//Get All the prime numbers between 1 to n
function printPrimes(n){
    for(let i = 2; i<=n; i++){

        if(isPrime(i)){
            console.log(i); 
        }
    }

}
//printPrimes(12);

 /*-----------------------------------------------------------------------*/


 //-------------------Another way to Print--------Using one function---------------------//

    function prime2(n){
        
        for(let i=2; i<=n; i++){
            let flag = 0;
            //console.log(i)
            for(j=2; j<i; j++){
                if(i % j == 0){
                    flag = 1;
                    break;
                }  
            }
            if( i > 1 && flag == 0){
                console.log(i);
            }
        }
    }
    prime2(11);
    