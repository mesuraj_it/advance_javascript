//helper Recursive function
/*
function in a parent function calls itself recursively called helper recursive function. nested function. child function helps the parent function to give output
*/
//[1,2,3,4,5,6,7,8,9] identify the odd numbers
//[1,3,5,7,9] => output
// number % 2!= 0  //1st logic

function findOdd(array){
    let result=[];

    function recursive(inputarray){
        console.log("Calling..."); //it will show how many times the helper function runs
        //recursion with an end point otherwise it will show "RangeError: Maximum call stack size exceeded"
        if(inputarray.length === 0){
            return;
        }
        if(inputarray[0] % 2!==0){
            result.push(inputarray[0]);
        }
        recursive(inputarray.slice(1));

    }
    recursive(array);
   return result;

}

const res = findOdd([1,2,3,4,5,6,7,8,9]);
console.log(res);