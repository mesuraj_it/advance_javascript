//checking Square in another array
//arr1[1,2,3,4],  arr2[1,4,9,16]
//if(arr1[i] * arr2[*] === arr2[j])
//let isSquare = true

/*------------------- 1st logic -------------------*/
function isSquareCheck(arr1,arr2){
    for(let i=0; i<arr1.length; i++){
        let isSquare = false;
        for(let j=0; j<arr2.length; j++){
            //console.log(arr2[j]);
            if(arr1[i] *arr1[i] === arr2[j]){
                isSquare = true;
            }
            //if it is not getting the square from arr2 then it returns false by default so that the condition we have to write
             if(j === arr2.length-1){
                if(!isSquare){
                    return false;
                }
             }
        }
    }
    return true;
}

const result = isSquareCheck([1,2,3,4], [1,4,9,16]);
//console.log(result);
//time complexity quadratic O(n^2)



/*-----------------second logic------------------*/
//array1=[1,2,4,2],         array2[1,4,4,16]

// JavaScriptmap{key:value}
//map1={1:1,2:2,4:1}        map2={1:1,4:2,16:1} 

function checkSquare(array1,array2){
    let map1={};
    let map2={};
    //map1
    for(item of array1){
        map1[item]=(map1[item] || 0)+1;
    }
    console.log(map1);
    //map2
    for(item2 of array2){
        map2[item2]=(map2[item2] || 0)+1;
    }
    console.log(map2);

    for(let key in map1){
        console.log("key", key);
        if(!map2[key * key]){   //map2 te square present a6ey ki na check kor6i
            return false;
        }
        if(map1[key] !== map2[key * key]){  //map1 er value map2 er square ki na check kor6i
            return false;
        }
    }
    return true;

}
//checkSquare([1,2,4,2], [1,4,4,16]);
const res = checkSquare([1,2,4,2], [1,4,4,25]);
console.log(res);
