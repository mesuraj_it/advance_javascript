// recursion : when a function call itself
// otherwise it will get called for infinite Time
// if a function call itself the there must be an end point
////inbuild function in javaScript--1.document.getElementById(), getElementByName()  2.JSON.parse()

function demo(){
    console.log("function call itself");
    demo()  //syntax of recursion function
}
//demo(); //call the function, it will run infinite times then shows error "maximum call stack size exceeded"


//recursion with end point
let count =1;
function demo2(num){
    if(count > num){
        return false;
    }
    console.log("function with end point"+count);
    count++;
    demo2(num);
}
//demo2(10);

/*-------------------- Advance Recursion function ------------------------- */
//Sorting using Recursion function
//[2,3,1,4] ==>By default case
//[2,1,3,4] => first sort
//[1,2,3,4] => final output

let myarray = [2,3,1,4];
let i=0;
let j=2;
let newlist =[];
//checking the array is sorted or not/ we can solve this without using this function but it is a structure of write standard logic or code
function isSorted(array){
    for(let i=0; i<array.length; i++){
        if(array[i] > array[i+1]){
            return false;
        }
    }
    return true;
}

function sorting(array){
    const res = isSorted(array);
    console.log(res);

    if(isSorted(array)){
        //return array; //if our array is not sorted for the first time then it will show undefined
         newlist = array;
         return;
    } else if(array[i] < array[j]){
        i++;
        j++;
        sorting(array);
    } else {
        [array[i],array[j]]=[array[j],array[i]]     //way to write swapping of two number
        i=0;
        j=1;
        sorting(array);        //call function in the function
    }
}
//const result = sort(myarray);
    //OR
//const result = sorting([2,3,1,4]);
//console.log(result);

//sorting([2,3,1,4]);
    //OR
 sorting(myarray); //first calling
console.log(newlist);

