var num1 = 34;
var num2 = 42;
//console.log(num1 + num2);

//data types in js
//number
var numb1 = 448;
var numb2 = 665;

//string
var str1 = "abc";
var str2 = "this is a string also";

//booleans
var a = true;
var b = false;
//console.log(a,b);

//undefined
var und1 = undefined;
var und2;
//onsole.log(und1,und2);

//null
var n = null;
//console.log(n);

/*there are two type of data types
1.preietive:undefined, null, number, string, boolean, symbol
2.Rrference data type: arrays and objects
*/
//array
var arr = [1, 2, 3, 4, 5]
var arr2 = [1, 2, "bablu", 4, "Santu"]
/*console.log(arr[0]);
console.log(arr[1]);
console.log(arr[2]);*/

//object
var marks = {
    ravi: 45,
    subham: 47,
    harry: 48.65
}
//console.log(marks);

/*Operators in javascript*/
//arithmetic operators
var a = 100;
var b = 10;
/*console.log("the value of a+b is", a+b);
console.log("the value of a-b is", a-b);
console.log("the value of a/b is", a/b);
console.log("the value of a*b is", a*b);*/

/*assignment operator*/
var c = b;
/*c+= 2;
c -= 3;
c*= 2
c /= 2
c++;
c--
console.log(c);*/

//comparison operator
var x = 34;
var y = 20;
/*console.log(x == y);
console.log(x >= y);
console.log(x <= y);
console.log(x > y);
console.log(x < y);*/

//logical operator//

//logical AND &&//
/*console.log(true && true);
console.log(true && false);
console.log(false && true);
console.log(false && false);*/

//logical OR
/*console.log(true || true);
console.log(true || false);
console.log(false || true);
console.log(false || false);*/

//logical not
/*console.log(!false);
console.log(!true);*/

//simple function in js
function avg(a, b) {
    //return(a+b)/2;

    c = (a + b) / 2; //you ca write like this
    return c;
}
//call the function
rs1 = avg(10, 20);
rs2 = avg(15, 55);
//console.log(rs1, rs2);

//conditional in js
//if else
var age = 20;
// if(age>10){
//     //console.log('you are not a kid');
// } else{
//     //console.log('you are  a kid');
// }

//if else if
if (age < 10) {
    //console.log('you are a kid');
} else if (age > 15) {
    //console.log('you are a school boy');
} else if (age > 18) {
    //console.log("You are matured now");
} else {
    // console.log("Bacche ha")
}
//console.log("End of ladder");


//loops in js
//for loop
var arr = [1, 2, 3, 4, 5, 6, 7];
//console.log(arr);
/*for(var i=0; i<arr.length; i++){
    console.log(arr[i]);
}*/

//you can write also like this using foreach loop
/*arr.forEach(function (element) {
    console.log(element);
})*/

//while loop
/*let j=0;
while(j<arr.length){
    console.log(arr[j]);
    j++;
}*/

//do while loop
/*let j = 0;
do{
    console.log(arr[j]);
    j++;
} while (j<arr.length);*/

/*for(var i=0; i<arr.length; i++){
    if(i==2){
        //break;
        continue;
    }
    console.log(arr[i]);
}*/

//array methods in JS
let myarr = ["fan", "camera", 34, "phone", null];
//console.log(myarr.length);
//myarr.pop();//one element will skipped from last
//myarr.shift() //first element will be skipped
//myarr.push("suraj");// one element will be added at last
//myarr.sort(); //short all the elements
//myarr.toString(); //convert all element to string
//console.log(myarr);

//String methods in JS
let myString = "Harry is a good boy";
// console.log(myString.length);
// console.log(myString.indexOf("good"));
// console.log(myString.slice(1,4));
myString.replace("Harry", "Rohan");
//console.log(myString);

//js time
let myDate = new Date();
// console.log(myDate.getTime());
// console.log(myDate.getFullYear());
// console.log(myDate.getDay());
// console.log(myDate.getHours());
// console.log(myDate.getMinutes());

//DOM Manipulation
let elem = document.getElementById('click');
//console.log(elem);
let elmclass = document.getElementsByClassName('container');
// console.log(elmclass);
// elmclass[0].style.background = "yellow"; //give style to a class
elmclass[0].classList.add("bg-primary"); //add a css class to a html class element
elmclass[0].classList.add("text-color");
//elmclass[0].classList.remove("text-color");//remove class
// console.log(elem.innerHTML);
// console.log(elem.innerText);
// console.log(elmclass[0].innerHTML);
// console.log(elmclass[0].innerText);
tn = document.getElementsByTagName('div');
console.log(tn);

createdElement = document.createElement('p');
createdElement.innerText = "This ia a created para";
tn[0].appendChild(createdElement); //it creates a new element(para) under first div(tn[0]) tag.

createdElement2 = document.createElement('b');
createdElement2.innerText = "This is replaced  Bold";
tn[0].replaceChild(createdElement2,createdElement);// replace child element


//Events in JS//
function clicked(){
    //console.log('The button was clicked')
}
window.onload = function(){
    //console.log('the document was loaded')
}
firstcontainer.addEventListener('click', function(){
    //console.log("Clicked on the button under firstcontainer");
})
firstcontainer.addEventListener('mouseover', function(){
    //console.log("mouse on firstcontainer");
})
firstcontainer.addEventListener('mouseout', function(){
    //console.log("mouse out from firstcontainer");
})
firstcontainer.addEventListener('mouseup', function(){
    //console.log("mouse up when clicked on firstcontainer");
})
firstcontainer.addEventListener('mousedown', function(){
    //console.log("mouse down when clicked on firstcontainer");
})
firstcontainer.addEventListener('click', function(){
    document.querySelectorAll('.container')[1].innerHTML="<b>we have clicked</b>"
    //console.log("Clicked on container");
})

// let prevHTML = document.querySelectorAll('.container')[1].innerHTML;
// firstcontainer.addEventListener('mouseup', function(){
//     document.querySelectorAll('.container')[1].innerHTML = prevHTML;
//     console.log("mouse up when clicked on container");
// })
// firstcontainer.addEventListener('mousedown', function(){
//     document.querySelectorAll('.container')[1].innerHTML="<b>we have clicked</b>"
//     console.log("Mouse down when clicked on container");
// })


//----------------arrow function----------------
//normal function
// function summ(a,b){
//     return a+b;
// }

summ = (a,b)=>{  //arrow function
    return a+b;
}

//setTimeout and setinterval
showtext = ()=>{
    document.querySelectorAll('.container')[1].innerHTML="<b>set Interval fired</b>"
    console.log("Welcome to the page");
}
//setTimeout(showtext, 3000); //showtext is a function name which we defined
//setInterval(showtext, 2000);

//use clearInterval(clr)/clearTimeout(clr) to cancel setInterval/setTimeout
// clr =setTimeout(showtext,6000);
// clr =setInterval(showtext,2000);

//JS local storage
localStorage.setItem('name','suraj');// we can set any value to localstorage
localStorage.getItem('name');//that will return the value what we set before
localStorage.removeItem('name'); //remove the item what we set before
localStorage.clear(); //remove or clear the whole item from our local storage

//------------json---------------------//
obj = {name:"harry", length:"5", a:{this:"that"}}
jso = JSON.stringify(obj);
console.log(typeof jso);
console.log(jso);

parsed = JSON.parse(`{"name":"harry", "length":"5", "a":{"this":"that"}}`)
console.log(parsed);





