//Higher order function -Technical suneja

//foreach, filter, map, sort, reduce

const companies =[
                {name:"google", cat:"Product", start:1981, end:2004},
                {name:"Amazon", cat:"Product", start:1992, end:2008},
                {name:"Paytm", cat:"Product", start:1986, end:2007},
                {name:"Coforge", cat:"Service", start:1994, end:2010},
                {name:"Mindtree", cat:"Service", start:1989, end:2010}
                ];
const ages =[33, 12, 20, 16, 5, 54, 21, 44, 61, 13, 15, 45, 25, 64, 32];

/*-----------------for---&---forEach--------------------*/
// iterate using standard for loop
    /*for(let i=0; i<=companies.length; i++){
        console.log(companies[i]);
    }*/

//iterate using forEach, forEach itself receives a callback, callback is two type synchronus and asynchronus
    /*companies.forEach(function(company,index) {
    //console.log(company);     // console.log(company.name) / console.log(index);
    })*/

//iterate using forEach arrow function
    /*companies.forEach((company, index) => {
        console.log(company);   //console.log(company.name) / console.log(index);
    })*/

// foreach loop just in one line
    /*companies.forEach((company, index) => (console.log(company)));  //console.log(company.name)/console.log(index)*/


/*------------------------Filter--------------------------*/
    //print age greater than 20
    /*for(let i=0; i<=ages.length; i++){
        if(ages[i]>=20){
            console.log(ages[i]);
        }
    }*/
    //using filter(). filter itself receives a callback function
   /*const age = ages.filter(function(age){
        if(age >= 20){
            return true;
        }
    })
    console.log(age);*/

    //filter arrow function
    /*const age = ages.filter((age)=> {
        if(age >= 20){
            return true;
        }
    })
    console.log(age);*/

    //just in one line
    //const finalage = ages.filter((age)=> (age>=20));    //   you can skip the parenthesis 
    const finalage = ages.filter(age=> age>=20);        //can pass more than one parameter like (age,index,.,.)
    //console.log(finalage)

    //filter only service based company
       /* const sb = companies.filter( (company)=> {
            if(company.cat === "Service"){
                return true;
            }
        })
        console.log(sb);*/
    //just in one line
   const sb = companies.filter(company=> company.cat === "Service");
    //console.log(sb);



/*---------------------Map()----------------------------------*/

       /* companies.map(function(company,index){
            console.log(company,index);
        })*/

        //map arrow function
        /*companies.map((company,index) => {
            console.log(company,index);
        })*/
        const demo = companies.map((company,index)=> {
            return `${company.name} ${company.cat}`     //string literals '${}'
        })
        //console.log(demo);

        //just in one line
        //companies.map((company,index) => console.log(company,index));



/*--------------------------Sorting-----------------------------*/
        //sort small to big, ascending order
        /*const sorted = companies.sort(function(c1,c2){
            if(c1.start > c2.start){        //if(c1.start < c2.start) descending order
                return 1;
            } else {
                return -1;
            }
        })
        console.log(sorted);*/
        //just in one line
        const sorted = companies.sort((c1,c2) => c1.start > c2.start ? 1 : -1);
        //console.log(sorted);

        //sort ages ascending order
        const sortedages = ages.sort((a,b) => (a - b));     //b-a descending order
        //console.log(sortedages);


/*--------------------------Reduce------------------------------*/
        //sum of all ages - Normal function using for loop
        /*let total =0;
        for(let i=0; i<ages.length; i++){
            total+=ages[i];
        }
        console.log(total);*/

        //using reduce function
        /*let total =0;
        const sumofage = ages.reduce((age,total) =>{
            return total+age;
        },0)
        console.log(sumofage);*/

        //just in one line
            const sumofage = ages.reduce((age,total)=> total+age,0);
            console.log(sumofage);        
