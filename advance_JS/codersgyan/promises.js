/*function register(callback){
    setTimeout( () => {
     console.log('Register End');
     callback();
    },2000)
     
 }*/

 function register(){
    return new Promise((resolve, reject) =>{
        setTimeout( () => {
            //return reject('Error while registering');
            console.log('Register End');
            resolve();
            //reject('Error while registering');  //show error messege 
        }, 2000);
    });
     
 }


function sendEmail(){
    return new Promise((resolve, reject) =>{
        setTimeout( () => {
            //return reject('Error while sending Email');     //error message
            console.log('Email End');
            resolve();
        }, 1000);
    });
    
}


function login (){
    return new Promise((resolve, reject) =>{
        setTimeout( () => {
            console.log('Login End');
            resolve();
        }, 3000)
    })
    
}


function getUserData(){
    return new Promise( (resolve, reject) =>{
        setTimeout( () => {
            console.log('Userdata Got');
            resolve();
        }, 1000);
    })
    
 }


 function displayUserData(){
    return new Promise( (resolve, reject) =>{
        setTimeout( () => {
            console.log('Display User data End');
            resolve();
        }, 1000);
    })

}
 





//then automatically call the function, we dont need to write sendEmail()
//.then access what we want to pass in resolve/success message
//.catch access what we pass in reject/error message
 /*register()
    .then(sendEmail)
    .then(login)
    .then(getUserData)
    .then(displayUserData)
        //error handling
        .catch((err) => {
            console.log('Error:', err);
        })*/
                   
  //Another way to use promises
 //Async Await, visually it seems synchronus but it works in the background asynchronus

 /*async function authenticate(){
    await register();
    await sendEmail();
    await login();
    await getUserData();
    await displayUserData();
 }
  //asyn await by default returns a promise
 authenticate().then(result =>{
    console.log('All set');
 }).catch(err =>{             //error handling in async await
    console.log(err);
 });*/

//Another way to catch error

async function authenticate(){
    try{
            await register();
            await sendEmail();
            await login();
            await getUserData();
            await displayUserData();
        //take in a variable to print success message
      /* const message = await register();
                        await sendEmail();
                        await login();
                        await getUserData();
                        await displayUserData();
            console.log('Success');*/

    } catch(err) {
        console.log(err);
        throw new Error();
    }
 }

 authenticate().then(result =>{
    console.log('All set');
 }).catch(error =>{             //error handling in async await
    console.log(err);
 });

 console.log('Other application work!');



 

