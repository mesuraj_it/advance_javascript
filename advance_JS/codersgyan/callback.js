//Senario
/* 
 *1.register
 *2.send welcome mail
 *3.login
 *4.get user data
 *5.display uesr data
*/

/*function waitTwoseconds(){
    let ms = 2000 + new Date().getTime();
    while(new Date() < ms){}
}
//this is the example of synchronus javaScript function
function register(){
    waitTwoseconds();
    console.log('Register End');
}

function sendEmail(){
    waitTwoseconds();
    console.log('Email End');
}

function login (){
    waitTwoseconds();
    console.log('Login End');
}

function getUserData(){
    waitTwoseconds();
    console.log('Userdata Got');
}

function displayUserData(){
    console.log('Display User data End');
}
// register();
// sendEmail();
// login();
// getUserData();
// displayUserData();
*/
/*--------------------------*/



//we can pass a function as a parameter, A function which can take another fuction as a parameter, is called a higher order function. register function is a higher order function
function register(callback){
   setTimeout( () => {
    console.log('Register End');
    callback();
   },2000)
    
}

function sendEmail(callback){
    setTimeout( () => {
        console.log('Email End');
        callback();
    },1000)
   
}

function login (callback){
    setTimeout( () => {
        console.log('Login End');
        callback();
    },3000)
    
}

function getUserData(callback){
    setTimeout( () => {
        console.log('Userdata Got');
        callback();
    },1000)
    
}

function displayUserData(){
    setTimeout( () => {
        console.log('Display User data End');
    },1000)
    
}

//nesting of function
//This is called callback hell. It's not the right way, this is old method. From here the Promises concept comes
register(function () {      //callback
    sendEmail(function(){       //callback
        login( function(){          //callback
            getUserData(function(){      //callback
                displayUserData();
            });          
        });
    });
});

console.log('Other application work!');