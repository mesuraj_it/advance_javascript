/*const p = new Promise(function(resolved, reject){
    //resolve()
    //reject()
})

p.then(function(data){

}).catch(function(data){

})*/

const p1 =new Promise((resolve, reject) =>{
    setTimeout(()=>{
        resolve('1')
        // reject('fail')
    },2000)
})

const p2 =new Promise((resolve, reject) =>{
    setTimeout(()=>{
        resolve('2')
        //reject('fail')
    },2000)
})

p1.then(data =>{
    console.log('then1',data);
    return p2
}).then(data =>{
    console.log('then2',data)
})
// .catch(err=> console.log(err));