
function register(){
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
            console.log("register end");
            resolve();
        },1000)
    })
   
}

function sendEmail(){
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            return reject("something went wrong");
            console.log("Email send")
        },2000)
    })
    
}

function login(){
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            console.log("Login successfully");
            resolve();
        },1000)
    })
    
}

/*register()
        .then(sendEmail)
        .then(login)
        .catch((error)=>{
            console.log(error);
        })*/

async function authenticate(){
    await register();
    await sendEmail();
    await login();
}
authenticate().then(()=>{
    console.log("All set");
}).catch((err)=>{
    console.log(err);
})