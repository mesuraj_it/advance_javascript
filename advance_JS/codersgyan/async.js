/*----------------Asynchronus javaScript by codersgyan-------------------*/

/*Event loop visualization: 
http://latentflip.com/loupe
you can check how fuction runs one by one on--- http://latentflip.com
Call Stack-if sync, if (event)async--->then go to Web Apis--->then go to Callback Queue and here check if it is empty or not, if empty then it goes to call stack 
All JS events are async function
*/

/*-----------Async Emample-1--Start----------------*/
//console.log('Hello');

/*setTimeout(function(){
    console.log('I am from settimeout');
},2000)*/
    //OR

function greeting(){
    console.log('i am from setTimeout..');
}
//setTimeout(greeting,2000); //we do not write greeting() bcoz it wil call the function

//console.log('async function');
/*------------END-----------------*/

/*-----------Another Emample-2----Start------------*/
console.log('start');
setTimeout( ()=>{
    console.log('async');
},0)
console.log('end');
/*---------Another Emample-2----End--------------*/