//when a function creates in javascript it will add an empty object on the function by default

function BankAccount(customername, balance = 0){
    this.customerName = customername;
    this.accountNumber = Date.now();
    this.balance = balance;

    // this.deposit = function (amount){
    //     this.balance += amount;
    // };

    // this.withdraw = (amount) => {
    //     this.balance -= amount;
    // };
}

 const johnAccount = new BankAccount("Rakesh");
// const johnAccount = new BankAccount('john doe', 1000)
// console.log(rakeshAccount,johnAccount);

BankAccount.prototype.deposit = function (amount){
    this.balance += amount;
};

BankAccount.prototype.withdraw = (amount) => {
    this.balance -= amount;
};

//johnAccount.deposit(3000);
console.log(johnAccount);
//console.log(BankAccount.prototype);
