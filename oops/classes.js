class BankAccount{
    customerName;
    accountNumber;
    balance = 0;

    constructor(customerName, balance = 0){
        this.customerName = customerName;
        this.accountNumber = Date.now();
        this.balance = balance;
    }


    deposit(amount){
        this.balance += amount;
    }

    withdraw(amount){
        this.balance -= amount;
    }
}

const rakeshAccount = new BankAccount('Rakesh K', 1000);
const johnAccount = new BankAccount('john doe');
rakeshAccount.deposit(4000);
johnAccount.deposit(2000);
console.log(rakeshAccount,johnAccount);


/*
//we can write function like this
const hello = function(){

} 
    //OR

function hello(){

}
//we can write class like this
    const BankAccount = class {

    }
        //
    class BankAccount {
        
    }

*/