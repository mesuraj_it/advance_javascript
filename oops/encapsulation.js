class BankAccount{
    customerName;
    accountNumber;
    #balance = 0;   //private property

    constructor(customerName, balance = 0){
        this.customerName = customerName;
        this.accountNumber = Date.now();
        this.#balance = balance;
    }

    deposit(amount){
        this.#balance += amount;
    }

    withdraw(amount){
        this.#balance -= amount;
    }
    //setter
    set balance(amount){
        if(isNaN(amount)){
            throw new Error('Amount is not valid number');
        }
        this.#balance = amount;
    }

    //getter
    get balance(){
        return this.#balance;
    }


}

class CurrentAccount extends BankAccount{
    transactionLimit = 50000;

    constructor(customerName, balance=0){
        super(customerName, balance)
    }

    //private method
    #calculateInterest(amount){
        console.log('calculating interest');
    }

    takeBusinessLoan(amount){
        this.#calculateInterest(amount);
        console.log('Taking business loan: ' + amount);
    }

}

const rakeshAccount = new CurrentAccount('Rakesh k',1000);
// rakeshAccount.balance = 5000 //this is the wrong way
// rakeshAccount.setBalance(400);
// rakeshAccount.balance = 5000;
// console.log(rakeshAccount.balance);

// rakeshAccount.calculateInterest(4000);
rakeshAccount.takeBusinessLoan(4000);
console.log(rakeshAccount);
