/*--------------------Inheritence Class syntax------------------------------------------*/
class BankAccount{
    customerName;
    accountNumber;
    balance = 0;

    constructor(customerName, balance = 0){
        this.customerName = customerName;
        this.accountNumber = Date.now();
        this.balance = balance;
    }

    deposit(amount){
        this.balance += amount;
    }

    withdraw(amount){
        this.balance -= amount;
    }
}

class CurrentAccount extends BankAccount{
    transactionLimit = 50000;

    constructor(customerName, balance=0){
        super(customerName, balance)
    }

    takeBusinessLoan(amount){
        console.log('Taking business loan: ' + amount);
    }

}



class SavingsAccount extends BankAccount{
    transactionLimit = 10000;

    constructor(customerName, balance=0){
        super(customerName, balance)
    }

    takePersonalLoan = function (amount){
        console.log('Taking personal loan: ' +  amount);
    };

}


const rakeshAccount = new SavingsAccount('Rakesh k', 500);
rakeshAccount.deposit(1500);
rakeshAccount.withdraw(500);
rakeshAccount.takePersonalLoan(40000);
console.log(rakeshAccount);





/*-----------------Inheritence Function Syntax----------------------------------*/
    /*function BankAccount(customername, balance = 0){
        this.customerName = customername;
        this.accountNumber = Date.now();
        this.balance = balance;
    }

    BankAccount.prototype.deposit = function (amount){
        this.balance += amount;
    };

    BankAccount.prototype.withdraw = function (amount) {
        this.balance -= amount;
    };*/
/*------------------current account-----------------------*/
    /*function CurrentAccount(customername, balance = 0){
        BankAccount.call(this,customername,balance);
        // this.customerName = customername;
        // this.accountNumber = Date.now();
        // this.balance = balance;
        this.transactionLimit = 50000; 
    }
    CurrentAccount.prototype = Object.create(BankAccount.prototype);

    CurrentAccount.prototype.takeBusinessLoan = function (amount){
        console.log('Taking business loan' + amount);
    }*/

    /*CurrentAccount.prototype.deposit = function (amount){
        this.balance += amount;
    };

    CurrentAccount.prototype.withdraw = (amount) => {
        this.balance -= amount;
    };*/
// const rakeshAccount = new CurrentAccount('Rakesh k', 500);
// console.log(rakeshAccount);


/*-------------------savings Account------------------------- */
    /*function SavingsAccount(customername, balance = 0){
        BankAccount.call(this,customername,balance)     //constructor linking, Inheritence
        // this.customerName = customername;
        // this.accountNumber = Date.now();
        // this.balance = balance;
        this.transactionLimit = 10000; 
    }

    SavingsAccount.prototype = Object.create(BankAccount.prototype);

    SavingsAccount.prototype.takePersonalLoan = function (amount){
        console.log('Taking personal loan' +  amount);
    };*/

/*SavingsAccount.prototype.deposit = function (amount){
    this.balance += amount;
};

SavingsAccount.prototype.withdraw = (amount) => {
    this.balance -= amount;
};*/

// const rakeshAccount = new SavingsAccount('Rakesh k', 500);
// rakeshAccount.deposit(500);
// rakeshAccount.withdraw(200);
// rakeshAccount.takePersonalLoan(40000);
// console.log(rakeshAccount);